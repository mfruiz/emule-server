# Emule-servers

File with spanish servers for Emule.

https://gitlab.com/mfruiz/emule-server/-/raw/main/server.met

## Sources

* https://www.mundodeportivo.com/urbantecno/tecnologia/los-mejores-servidores-para-emule

* http://sites.google.com/site/ircemulespanish/descargas-2/server.met

## Servers

* eMule Security / IP: 80.208.228.241 / Puert: 8369
* GrupoTS Server / IP: 46.105.126.71 / Puerto: 4661
* !! Sharing-Devils No.1 !! / IP: 91.208.184.143 / Puerto: 4232
* !! Sharing-Devils No.2 !! / IP: 94.23.97.30 / Puerto: 4245
* La Cosa Nostra / IP: 94.23.97.30 / Puerto: 4242
